@Imports DotNet_MicroService_Template.Areas.HelpPage.ModelDescriptions
@ModelType ComplexTypeModelDescription
@Html.DisplayFor(Function(m) Model.Properties, "Parameters")
